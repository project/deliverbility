EmailChecker
----------------
This is a drupal 7 module to enable the email verification and validation functionality for the needed forms / fields.

Author:
---------
Name : Deliverbility
Site : https://deliverbility.com

Download and Install:
-------------------------
1. Download the Deliverbility drupal 7 module, extract it and place it under sites/all/modules/. For example, if /var/www/html is your web server document root, unzip unto /var/www/html/sites/all/modules/
2. Enable Deliverbility module (/admin/modules).


Configure:
---------------
1. Goto the Deliverbility configuration form
    1.1 API Key => Place your API access key

    1.2 Form and Field Details => Configure your form and fields, it should be in the "form_id|field_id" format.

          for example :
          **************
          user_register_form|mail
          contact_site_form|mail
          contact_personal_form|mail

    1.3 Enable Log => Enable logging for your results.


Your form will now automatically be secured with the deliverbility.com verification API.
